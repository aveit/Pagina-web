import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarioModule } from './calendario/calendario.module';
import { ComponentesModule } from './componentes/componentes.module';

// Librerias para el calendario
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

// Perfect Scrollbar
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// ReCaptcha
// https://github.com/DethAriel/ng-recaptcha
import { RecaptchaV3Module, RECAPTCHA_V3_SITE_KEY } from 'ng-recaptcha';

// Servicios
import { SociosApiService } from './_services/socios-api.service';

// Componentes
import { InicioComponent } from './inicio/inicio.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ViajeComponent } from './viaje/viaje.component';
import { QueEsAveitComponent } from './que-es-aveit/que-es-aveit.component';
import { PreinscripcionComponent } from './preinscripcion/preinscripcion.component';
import { VerifEmailComponent } from './verif-email/verif-email.component';
import { CulturaAveitComponent } from './cultura-aveit/cultura-aveit.component';
import { RifaComponent } from './rifa/rifa.component';
import { NuestraHistoriaComponent } from './nuestra-historia/nuestra-historia.component';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    NosotrosComponent,
    ViajeComponent,
    QueEsAveitComponent,
    PreinscripcionComponent,
    VerifEmailComponent,
    CulturaAveitComponent,
    RifaComponent,
    NuestraHistoriaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    CalendarioModule,
    ComponentesModule,
    PerfectScrollbarModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}), // Agrega el spinner de loading
    RecaptchaV3Module
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    SociosApiService,
    { provide: RECAPTCHA_V3_SITE_KEY, useValue: "6LcO580aAAAAANdIdecSdQwuYdMu6K0mFCyOYKVA" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
  