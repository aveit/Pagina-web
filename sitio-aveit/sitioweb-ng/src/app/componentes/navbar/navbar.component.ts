import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.scrollTo({top: 0}); // Cuando inicia el navbar, lleva a arriba de todo
  }

  scroll(el: string) {
    let elemento = <HTMLElement>document.getElementById(el);
    window.scrollTo({top: elemento.offsetTop - 50, behavior: 'smooth'});
  }
}
