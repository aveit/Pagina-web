import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-cultura-aveit',
  templateUrl: './cultura-aveit.component.html',
  styleUrls: ['./cultura-aveit.component.scss']
})
export class CulturaAveitComponent implements OnInit {

  public scrolledSerSocio = 0;

  constructor() { }

  ngOnInit(): void {
  }

  @HostListener('window:scroll', ['$event']) // Se detecta el scrolleo. Cuando se llega a un punto, se agrega
  onWindowScroll() { // la clase animada (css) al collage
    const numb = window.scrollY;
    let serSocio: HTMLElement = <HTMLElement>document.getElementById("serSocio");

    if((numb >= (serSocio.offsetTop-(window.innerHeight/1.2))) && (this.scrolledSerSocio == 0)) {
      this.scrolledSerSocio = 1;
    }

  }

  modal(im: string) {
    let modal = <HTMLDivElement>document.getElementById("myModal");
  
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    let img = <HTMLImageElement>document.getElementById(im);
    let modalImg = <HTMLImageElement>document.getElementById("img01");
    let captionText = <HTMLDivElement>document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // console.log(img);

    // let span = <HTMLSpanElement>document.getElementsByClassName("close")[0];
  }

  cerrarModal() {
    let modal = <HTMLDivElement>document.getElementById("myModal");
    modal.style.display = "none";
  }

  modalDos(im: string) {
    let modal = <HTMLDivElement>document.getElementById("myModalDos");
  
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    
    let modalImg = <HTMLImageElement>document.getElementById("img02");

    let img = <HTMLDivElement>document.getElementById(im);
    // let style = img.currentStyle || window.getComputedStyle(img, false);
    
    let bi = img.style.backgroundImage.slice(4, -1).replace(/"/g, "");
    console.log(bi);

    modal.style.display = "block";
    modalImg.src = bi;
    // captionText.innerHTML = img.attributes.;
    // console.log(img);

    // let span = <HTMLSpanElement>document.getElementsByClassName("close")[0];
  }

  cerrarModalDos() {
    let modal = <HTMLDivElement>document.getElementById("myModalDos");
    modal.style.display = "none";
  }

}
