import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery';

declare var linea: Function;

@Component({
  selector: 'app-nuestra-historia',
  templateUrl: './nuestra-historia.component.html',
  styleUrls: ['./nuestra-historia.component.scss']
})
export class NuestraHistoriaComponent implements OnInit {

  constructor(private router: Router) { 
  }

  ngOnInit(): void {
    linea();
  }
}