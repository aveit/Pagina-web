import { Component, OnInit, HostListener } from '@angular/core';
import { element } from 'protractor';

@Component({
  selector: 'app-viaje',
  templateUrl: './viaje.component.html',
  styleUrls: ['./viaje.component.scss']
})
export class ViajeComponent implements OnInit {

  public esTel: boolean = false;
  public scrolledCollage = 0;
  public scrolledHist1 = 0;
  public scrolledHist2 = 0;
  public scrolledHist3 = 0;
  public scrolledFrase = 0;

  constructor() { }

  ngOnInit(): void {
    let fondoViajeAnios = <HTMLDivElement>document.getElementById("fondoViajeAnios");
    let frase = <HTMLDivElement>document.getElementById("frase");
    let cabecera = <HTMLDivElement>document.getElementById("cabecera");
    if(window.innerWidth < 750) { // Si es un dispositivo movil (Telefono, tablet, etc.) no carga el video
      this.esTel = true;
      fondoViajeAnios.style.backgroundImage = "none";
      frase.style.backgroundImage = "none";
    }
  }

  @HostListener('window:scroll', ['$event']) // Se detecta el scrolleo. Cuando se llega a un punto, se agrega
  onWindowScroll() { // la clase animada (css) al collage
    const numb = window.scrollY;
    let hist1: HTMLElement = <HTMLElement>document.getElementById("hist1");
    let hist2: HTMLElement = <HTMLElement>document.getElementById("hist2");
    let hist3: HTMLElement = <HTMLElement>document.getElementById("hist3");
    let frase: HTMLElement = <HTMLElement>document.getElementById("frase");

    if ((numb >= 500) && (this.scrolledCollage == 0)) {
      this.scrolledCollage = 1;
    }

    if((numb >= (hist1.offsetTop-(window.innerHeight/2))) && (this.scrolledHist1 == 0)) {
      this.scrolledHist1 = 1;
    }

    if((numb >= (hist2.offsetTop-(window.innerHeight/2))) && (this.scrolledHist2 == 0)) {
      this.scrolledHist2 = 1;
    }

    if((numb >= (hist3.offsetTop-(window.innerHeight/2))) && (this.scrolledHist3 == 0)) {
      this.scrolledHist3 = 1;
    }

    if ((numb >= (frase.offsetTop-(window.innerHeight/2))) && (this.scrolledFrase == 0)) {
      this.scrolledFrase = 1;
    }
  }

  scroll(el: HTMLElement) {
    window.scrollTo({top: el.offsetTop - 50, behavior: 'smooth'});
  }

  modal(im: string) {
    let modal = <HTMLDivElement>document.getElementById("myModal");
  
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    let img = <HTMLImageElement>document.getElementById(im);
    let modalImg = <HTMLImageElement>document.getElementById("img01");
    let captionText = <HTMLDivElement>document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // console.log(img);

    // let span = <HTMLSpanElement>document.getElementsByClassName("close")[0];
  }

  cerrarModal() {
    let modal = <HTMLDivElement>document.getElementById("myModal");
    modal.style.display = "none";
  }
}
