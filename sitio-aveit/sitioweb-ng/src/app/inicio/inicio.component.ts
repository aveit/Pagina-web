import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {

  public anio: number = new Date().getFullYear();

  constructor() { }

  ngOnInit(): void {
  }

  scroll(el: HTMLElement) {
    window.scrollTo({top: el.offsetTop - 50, behavior: 'smooth'});
  }

  modal(im: string) {
    let modal = <HTMLDivElement>document.getElementById("myModal");
  
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    let img = <HTMLImageElement>document.getElementById(im);
    let modalImg = <HTMLImageElement>document.getElementById("img01");
    let captionText = <HTMLDivElement>document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // console.log(img);

    // let span = <HTMLSpanElement>document.getElementsByClassName("close")[0];
  }

  cerrarModal() {
    let modal = <HTMLDivElement>document.getElementById("myModal");
    modal.style.display = "none";
  }

  modalDos(im: string) {
    let modal = <HTMLDivElement>document.getElementById("myModalDos");
  
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    
    let modalImg = <HTMLImageElement>document.getElementById("img02");

    let img = <HTMLDivElement>document.getElementById(im);
    // let style = img.currentStyle || window.getComputedStyle(img, false);
    
    let bi = img.style.backgroundImage.slice(4, -1).replace(/"/g, "");
    console.log(bi);

    modal.style.display = "block";
    modalImg.src = bi;
    // captionText.innerHTML = img.attributes.;
    // console.log(img);

    // let span = <HTMLSpanElement>document.getElementsByClassName("close")[0];
  }

  cerrarModalDos() {
    let modal = <HTMLDivElement>document.getElementById("myModalDos");
    modal.style.display = "none";
  }
}