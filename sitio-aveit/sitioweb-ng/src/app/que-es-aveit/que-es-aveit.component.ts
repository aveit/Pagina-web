import { Component, OnInit, HostListener } from '@angular/core';
import { element } from 'protractor';

@Component({
  selector: 'app-que-es-aveit',
  templateUrl: './que-es-aveit.component.html',
  styleUrls: ['./que-es-aveit.component.scss']
})
export class QueEsAveitComponent implements OnInit {

  public anio: number = new Date().getFullYear();
  
  public scrolledEventos = 0;
  public scrolledCrecimiento = 0;
  public scrolledSolidarias = 0;
  public scrolledSocios = 0;

  constructor() { }

  ngOnInit(): void {
    window.scrollTo({top: 0});
  }

  @HostListener('window:scroll', ['$event']) // Se detecta el scrolleo. Cuando se llega a un punto, se agrega
  onWindowScroll() { // la clase animada (css) al collage
    const numb = window.scrollY;
    let eventos: HTMLElement = <HTMLElement>document.getElementById("eventos");
    let crecimiento: HTMLElement = <HTMLElement>document.getElementById("crecimiento");
    let solidarias: HTMLElement = <HTMLElement>document.getElementById("solidarias");
    let socios: HTMLElement = <HTMLElement>document.getElementById("socios")

    if((numb >= (eventos.offsetTop-(window.innerHeight/1.2))) && (this.scrolledEventos == 0)) {
      this.scrolledEventos = 1;
    }
    if((numb >= (crecimiento.offsetTop-(window.innerHeight/1.2))) && (this.scrolledCrecimiento == 0)) {
      this.scrolledCrecimiento = 1;
    }
    if((numb >= (solidarias.offsetTop-(window.innerHeight/1.2))) && (this.scrolledSolidarias == 0)) {
      this.scrolledSolidarias = 1;  
    }
    if((numb >= (socios.offsetTop-(window.innerHeight/2))) && (this.scrolledSocios == 0)) {
      this.scrolledSocios = 1;
    }
  }

  scroll(el: HTMLElement) {
    window.scrollTo({top: el.offsetTop - 50, behavior: 'smooth'});
  }

  modal(im: string) {
    let modal = <HTMLDivElement>document.getElementById("myModal");
  
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    let img = <HTMLImageElement>document.getElementById(im);
    let modalImg = <HTMLImageElement>document.getElementById("img01");
    let captionText = <HTMLDivElement>document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // console.log(img);

    // let span = <HTMLSpanElement>document.getElementsByClassName("close")[0];
  }

  cerrarModal() {
    let modal = <HTMLDivElement>document.getElementById("myModal");
    modal.style.display = "none";
  }
}
