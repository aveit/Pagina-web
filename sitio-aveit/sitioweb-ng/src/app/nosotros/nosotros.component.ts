import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.scss']
})
export class NosotrosComponent implements OnInit {

  public anio: number = new Date().getFullYear();
  public scrolledCD = 0;
  public scrolledTribunal = 0;
  public scrolledSubcomisiones = 0;

  constructor() { }

  ngOnInit(): void {
  }

  @HostListener('window:scroll', ['$event']) // Se detecta el scrolleo. Cuando se llega a un punto, se agrega
  onWindowScroll() { // la clase animada (css) al collage
    const numb = window.scrollY;
    let comisionDirectiva: HTMLElement = <HTMLElement>document.getElementById("comisionDirectiva");
    let tribunal: HTMLElement = <HTMLElement>document.getElementById("tribunal");
    let subcomisiones: HTMLElement = <HTMLElement>document.getElementById("subcomisiones");

    if((numb >= (comisionDirectiva.offsetTop-(window.innerHeight/1.2))) && (this.scrolledCD == 0)) {
      this.scrolledCD = 1;
    }
    if((numb >= (tribunal.offsetTop-(window.innerHeight/1.2))) && (this.scrolledTribunal == 0)) {
      this.scrolledTribunal = 1;
    }
    if((numb >= (subcomisiones.offsetTop-(window.innerHeight/1.2))) && (this.scrolledSubcomisiones == 0)) {
      this.scrolledSubcomisiones = 1;  
    }

  }


  scroll(el: HTMLElement) {
    window.scrollTo({top: el.offsetTop - 50, behavior: 'smooth'});
  }

}
