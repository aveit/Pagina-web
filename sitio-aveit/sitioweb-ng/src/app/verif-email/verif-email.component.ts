import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SociosApiService } from '../_services/socios-api.service';
import { ConfigSolicitudesPreinscripcion } from '../_interfaces/socios';

@Component({
  selector: 'app-verif-email',
  templateUrl: './verif-email.component.html',
  styleUrls: ['./verif-email.component.scss']
})
export class VerifEmailComponent implements OnInit {

  public anio: number = new Date().getFullYear();
  private configPreinsc: ConfigSolicitudesPreinscripcion[] = [];
  private obs;
  private token: string = "";
  public verif: any;
  public mensaje: string = "";
  public submensaje: string = "";

  constructor(private activatedRoute: ActivatedRoute, private sociosApi: SociosApiService,
              private router: Router) { 
    this.obs = this.activatedRoute.queryParams.subscribe(params => {
      this.token = params['id'];
    });
  }

  ngOnInit(): void {
    this.sociosApi.getConfigSolicitudesPreinscripcion().then(
      (resp) => {
        this.configPreinsc = resp;
        if(this.configPreinsc[1].configuracion == 1) { // Si la verificacion esta desactivada, no hace la request
          this.sociosApi.verificarEmail(this.token).then(
            (resp) => {
              this.verif = resp;
              this.mensaje = "¡Tu e-mail fue verificado con éxito y ya estas preinscripto!";
              this.submensaje = "Recordá que tu preinscripción aún no es definitiva y se realizará una auditoría de tu condición académica";
            }
          ).catch(
            (error) => {
              this.verif = error;
              this.mensaje = "Error";
              if(this.verif.status == 400) {
                this.submensaje = "Tu e-mail ya ha sido verificado con anterioridad";
              }

              else if(this.verif.status == 403 || this.verif.status == 404) {
                this.submensaje = "Enlace inválido";
              }
            }
          );
        }

        else {
          this.obs.unsubscribe();
          this.mensaje = "Error"
          this.submensaje = "La verificación se encuentra deshabilitada";
          setTimeout(
            () => {
              this.router.navigate(['/']);
            }, 4000
          );
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.obs.unsubscribe();
  }
}
