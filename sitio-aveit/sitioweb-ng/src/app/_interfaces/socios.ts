export interface TiposMedios {
    codMedio: number;
    nombre: string;
}

export interface TiposEspecialidades {
    codEspecialidad: number;
    nombre: string;
}

export interface ConfigSolicitudesPreinscripcion {
    idConfig: number;
    nombre: string;
    configuracion: number;
}