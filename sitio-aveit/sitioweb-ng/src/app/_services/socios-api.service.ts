import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { TiposMedios, TiposEspecialidades, ConfigSolicitudesPreinscripcion } from '../_interfaces/socios';

@Injectable({
  providedIn: 'root'
})
export class SociosApiService {
  private url: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  nuevaSolicitudPreinscripto(nombre: string, apellido: string, codEspecialidad: string,
    legajo: string, email: string, emailAcademico: string, celular: string,
    codMedio: string, descripcionMedio: string, tokenCaptcha: string) {
    let obj: any = {
      nombre: nombre,
      apellido: apellido,
      codEspecialidad: codEspecialidad,
      legajo: legajo,
      email: email,
      emailAcademico: emailAcademico,
      celular: celular,
      codMedio: codMedio,
      descripcionMedio: descripcionMedio,
      tokenCaptcha: tokenCaptcha
    };

    return this.http.post(this.url + '/socios/preinscripto/solicitud', obj).toPromise();
  }

  getTiposMedios(): Promise<TiposMedios[]> {
    return this.http.get<TiposMedios[]>(this.url + '/socios/medios/').toPromise();
  }

  getTiposEspecialidades(): Promise<TiposEspecialidades[]> {
    return this.http.get<TiposEspecialidades[]>(this.url + '/socios/especialidades/').toPromise();
  }

  verificarEmail(token: string) {
    return this.http.get(this.url + '/socios/preinscripto/verificar/' + token).toPromise();
  }

  getConfigSolicitudesPreinscripcion(): Promise<ConfigSolicitudesPreinscripcion[]> {
    return this.http.get<ConfigSolicitudesPreinscripcion[]>(this.url + '/configuraciones/').toPromise();
  }
}
