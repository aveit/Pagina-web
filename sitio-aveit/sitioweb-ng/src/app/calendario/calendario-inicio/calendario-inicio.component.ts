import { Component, OnInit, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { CalendarEvent, CalendarView } from 'angular-calendar';
import { colors } from '../utils/colors';
// import { CalendarHeaderComponent } from '../utils/calendar-header.component';
import { addDays, isSameDay, isSameMonth } from 'date-fns';

@Component({
  selector: 'app-calendario-inicio',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None, // hack to get the styles to apply locally
  templateUrl: './calendario-inicio.component.html',
  styleUrls: ['./calendario-inicio.component.scss']
})
export class CalendarioInicioComponent implements OnInit {

  public view: CalendarView = CalendarView.Month;
  public viewDate: Date = new Date();
  public locale: string = "es";
  public activeDayIsOpen: boolean = false;

  constructor() { }

  ngOnInit(): void {
    for(let i: number = 0; i < this.events.length; i++) { // Este for recorre y verifica si la fecha de uno de los eventos coincide con la de hoy
      if(isSameDay(this.events[i].start, new Date())) { // En ese caso, abre y muestra los eventos del dia. Si no, no abre
        this.activeDayIsOpen = true;
        break; // A la primera coincidencia, sale del for.
      }
    }
  }

  events: CalendarEvent[] = [
    {
      title: 'EVENTASO',
      color: colors.yellow,
      start: addDays(new Date(), 0),
      cssClass: 'calendario-inicio.component',
    },
    {
      title: 'Eventeitor',
      color: colors.red,
      start: addDays(new Date(), 0),
      cssClass: 'calendario-inicio.component',
    }
  ];

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
}
