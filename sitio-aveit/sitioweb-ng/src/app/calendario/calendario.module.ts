import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarioInicioComponent } from './calendario-inicio/calendario-inicio.component';
import { CalendarModule } from 'angular-calendar';
import { CalendarHeaderComponent } from './utils/calendar-header.component';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';

registerLocaleData(localeEs);

@NgModule({
  declarations: [CalendarioInicioComponent, CalendarHeaderComponent],
  imports: [
    CommonModule,
    CalendarModule
  ],
  exports: [CalendarHeaderComponent, CalendarioInicioComponent]
})
export class CalendarioModule { }
