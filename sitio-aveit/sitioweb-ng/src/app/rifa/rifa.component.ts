import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-rifa',
  templateUrl: './rifa.component.html',
  styleUrls: ['./rifa.component.scss']
})
export class RifaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  modal(im: string) {
    let modal = <HTMLDivElement>document.getElementById("myModal");
  
    // Get the image and insert it inside the modal - use its "alt" text as a caption
    let img = <HTMLImageElement>document.getElementById(im);
    let modalImg = <HTMLImageElement>document.getElementById("img01");
    let captionText = <HTMLDivElement>document.getElementById("caption");

    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = img.alt;
    // console.log(img);

    // let span = <HTMLSpanElement>document.getElementsByClassName("close")[0];
  }

  cerrarModal() {
    let modal = <HTMLDivElement>document.getElementById("myModal");
    modal.style.display = "none";
  }

}
