import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { TiposMedios, TiposEspecialidades, ConfigSolicitudesPreinscripcion } from '../_interfaces/socios';

import { SociosApiService } from '../_services/socios-api.service';

import { ReCaptchaV3Service } from 'ng-recaptcha';
import { Subscription } from 'rxjs';
// import * as $ from 'jquery';

@Component({
  selector: 'app-preinscripcion',
  templateUrl: './preinscripcion.component.html',
  styleUrls: ['./preinscripcion.component.scss']
})
export class PreinscripcionComponent implements OnInit {

  private animaciones: boolean = true; // Permite habilitar o deshabilitar las animaciones
  private titulo = <HTMLDivElement>document.getElementById('titulo');
  private formulario = <HTMLDivElement>document.getElementById('formulario');
  private informacion = <HTMLDivElement>document.getElementById('informacion');
  private mensajeExito = <HTMLDivElement>document.getElementById('mensajeExito');
  private suscripcionCaptcha = new Subscription;
  private token: string = "";

  public anio: number = new Date().getFullYear();
  public permitirAcceso: boolean = false;
  public configsPreinsc: ConfigSolicitudesPreinscripcion[] = [];
  public loading: boolean = false;
  public entradaInfo: boolean = false;
  public entradaForm: boolean = false;
  public medios: TiposMedios[] = [];
  public especialidades: TiposEspecialidades[] = [];
  public error: boolean = false;
  public mensajeError: string = "";
  public exito: boolean = false;
  public desactBotonSolicitar: boolean = false;
  public emailAcad: string = "";

  public formPreinsc = new FormGroup({
    nombre: new FormControl(""),
    apellido: new FormControl(""),
    codEspecialidad: new FormControl(""),
    legajo: new FormControl(""),
    email: new FormControl(""),
    emailAcademico: new FormControl(""),
    repeEmailAcademico: new FormControl(""),
    celular: new FormControl(""),
    codMedio: new FormControl(""),
    descripcionMedio: new FormControl("")
  });

  constructor(private sociosApi: SociosApiService,
              private recaptchaV3Service: ReCaptchaV3Service,
              private router: Router) { }

  ngOnInit(): void {
    this.sociosApi.getConfigSolicitudesPreinscripcion().then(
      (resp) => {
        this.configsPreinsc = resp;
        if(this.configsPreinsc[0].configuracion == 1) {
          this.permitirAcceso = true;
          window.scrollTo({top: 0});
          this.titulo = <HTMLDivElement>document.getElementById('titulo');
          this.informacion = <HTMLDivElement>document.getElementById('informacion');
          this.formulario = <HTMLDivElement>document.getElementById('formulario');
          this.mensajeExito = <HTMLDivElement>document.getElementById('mensajeExito');

          if(this.animaciones) {
            this.informacion.style.opacity = "0";
            this.formulario.style.opacity = "0";
            this.mensajeExito.style.opacity = "0";

            setTimeout(() => {
              this.titulo.style.opacity = "0";
              this.titulo.classList.add('titulo');
              setTimeout(() => {
                this.titulo.outerHTML = "";
                this.entradaInfo = true;
                this.informacion.style.opacity = "1";
                this.informacion.classList.add('titulo');
              }, 1000);
            }, 1300);
          }

          else {
            let titulo = <HTMLDivElement>document.getElementById('titulo');
            let formulario = <HTMLDivElement>document.getElementById('formulario');
            titulo.hidden = true;
            this.entradaForm = true;
            formulario.style.opacity = "1";
          }

          this.sociosApi.getTiposMedios().then(
            (resp) => {
              this.medios = resp;
            }
          );

          this.sociosApi.getTiposEspecialidades().then(
            (resp) => {
              this.especialidades = resp;
            }
          );
        }

        else {
          this.router.navigate(["/"]);
        }
      }
    );
  }

  heLeido() {
    setTimeout(() => {
      this.informacion.style.opacity = "0";
      this.informacion.classList.add('exitoso');
      setTimeout(() => {
        this.informacion.outerHTML = "";
        this.entradaForm = true;
        this.formulario.style.opacity = "1";
        this.formulario.classList.add('titulo');
        window.scrollTo({top: 0});
      }, 400);
    }, 1);
  }

  solicitarPreInscripcion(): void {
    let nombre: string = this.formPreinsc.value['nombre'];
    let apellido: string = this.formPreinsc.value['apellido'];
    let codEspecialidad: string = this.formPreinsc.value['codEspecialidad'];
    let legajo: string = this.formPreinsc.value['legajo'];
    let email: string = this.formPreinsc.value['email'];
    let emailAcademico: string = this.formPreinsc.value['emailAcademico'];
    let repeEmailAcademico: string = this.formPreinsc.value['repeEmailAcademico'];
    let celular: string = this.formPreinsc.value['celular'];
    let codMedio: string = this.formPreinsc.value['codMedio'];
    let descripcionMedio: string = this.formPreinsc.value['descripcionMedio'];

    let nombreRegex: RegExp = /^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/; // Espresion regular para nombre
    // let apellidoRegex: RegExp = /^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]+$/g; // Espresion regular para apellido. Por alguna fucking razon no se puede usar el mismo regex dos veces
    let legajoRegex: RegExp = /^[0-9]{5,6}/;
    let emailRegex: RegExp = /^ *[_a-zA-Z0-9]+(\.[_a-zA-Z0-9]+)*@gmail\.com *$/; // regex para email comun
    let emailAcadRegex: RegExp = /^ *[0-9]{5,6}@[a-zA-Z]{5,11}(\.frc\.utn\.edu\.ar) *$/; // Regex para el mail de la facu
    let repeEmailAcadRegex: RegExp = /^ *[0-9]{5,6}@[a-zA-Z]{5,11}(\.frc\.utn\.edu\.ar) *$/; // Regex para el mail de la facu
    let celularRegex: RegExp = /^\+?[0-9]{9,17}$/; // Matchea numeros de telefono con o sin el + adelante
    let descrMedioRegex: RegExp = /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{0,29}$/im; // Regex para la descripcion del medio

    this.error = false;
    
    if(nombre == "" || nombre == undefined || apellido == "" || apellido == undefined || codEspecialidad == "" || codEspecialidad == undefined || legajo == "" || 
    legajo == undefined || email == "" || email == undefined || emailAcademico == "" || emailAcademico == undefined || repeEmailAcademico == "" || 
    repeEmailAcademico == undefined || celular == "" || celular == undefined || codMedio == "" || codMedio == undefined || descripcionMedio == "" || descripcionMedio == undefined) {
      this.mensajeError = "¡Error! Datos incompletos";
      this.error = true;
    }

    else if(!nombreRegex.test(nombre)) {
      this.mensajeError = "¡Error! Nombre inválido";
      this.error = true;
    }

    else if(!nombreRegex.test(apellido)) {
      this.mensajeError = "¡Error! Apellido inválido";
      this.error = true;
    }

    else if(!emailRegex.test(email)) {
      this.mensajeError = "¡Error! E-mail personal inválido";
      this.error = true;
    }

    else if(!emailAcadRegex.test(emailAcademico) || !repeEmailAcadRegex.test(repeEmailAcademico)) {
      this.mensajeError = "¡Error! E-mail académico inválido";
      this.error = true;
    }

    else if(emailAcademico != repeEmailAcademico) {
      this.mensajeError = "¡Error! Los e-mails académicos no coinciden";
      this.error = true;
    }

    else if(!legajoRegex.test(legajo)) {
      this.mensajeError = "¡Error! Número de legajo inválido";
      this.error = true;
    }

    else if(!celularRegex.test(celular)) {
      this.mensajeError = "¡Error! Número de teléfono inválido";
      this.error = true;
    }

    else if(!descrMedioRegex.test(descripcionMedio)) {
      this.mensajeError = "¡Error! Nombre de usuario de red social inválido";
      this.error = true;
    }

    else if(!this.evaluarEspecialidad(emailAcademico)) {
      this.mensajeError = "¡Error! E-mail inválido, debes ser estudiante de ingeniería";
      this.error = true;
    }

    else if(!this.evaluarLegajoEmail(legajo, codEspecialidad, emailAcademico)) {
      this.mensajeError = "¡Error! Tus datos no coinciden con tu e-mail académico";
      this.error = true;
    }

    else {
      this.desactBotonSolicitar = true;
      this.loading = true;
      this.suscripcionCaptcha = this.recaptchaV3Service.execute("solicitarPreinscripcion").subscribe(
        (token) => {
          this.token = token; // Token del captcha
          this.sociosApi.nuevaSolicitudPreinscripto(nombre, apellido, codEspecialidad, legajo, email, 
            emailAcademico, celular, codMedio, descripcionMedio, this.token).then(
            (resp) => {
              let c: string = emailAcademico.substring(0, emailAcademico.indexOf("@")+1) + ".." + emailAcademico.substring(emailAcademico.indexOf("."));
              this.emailAcad = c;
              
              setTimeout(() => { // Animacion de salida del form y entrada del mensaje de exito
                this.formulario.style.opacity = "0";
                this.formulario.classList.add('exitoso');
                setTimeout(() => {
                  this.loading = false;
                  this.formulario.innerHTML = "";
                  this.exito = true;
                  this.mensajeExito.style.opacity = "1";
                  this.mensajeExito.classList.add('titulo');
                }, 800);
              }, 500);
            }
          ).catch(
            (error) => {
              if(error.status == 401) {
                this.mensajeError = "¡Error! Ya te has registrado";
                this.error = true;
                this.loading = false;
                this.desactBotonSolicitar = false;
              }
            }
          );
        },
        (error) => {
          this.token = "";
          console.log("No era por ahi");
        }
      );
    }
  }

  evaluarEspecialidad(emailAcad: string): boolean {
    // Evalua si el email corresponde a una ingenieria
    let c: string = emailAcad.substring(emailAcad.indexOf("@")+1, emailAcad.indexOf(".")); // Crea una subcadena solo con la carrera en el mail
    
    for(let i of this.especialidades) {
      if(i.nombre.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase() === c) { // Las funciones normalize y replace eliminan los acentos y la funcion toLowerCase pone todo a minusculas
        return true;
      }
    }
    return false;
  }

  evaluarLegajoEmail(legajo: string, espec: string, emailAcad: string): boolean {
    // Evalua si el legajo y la carrera ingresados y el mail academico coinciden
    let c: string = emailAcad.substring(emailAcad.indexOf("@")+1, emailAcad.indexOf(".")); // Crea una subcadena solo con la carrera en el mail
    let l: string = emailAcad.substring(0, emailAcad.indexOf("@"));

    if(legajo !== l) {
      return false;
    }

    for(let i of this.especialidades) {
      if(String(i.codEspecialidad) === espec) {
        if(i.nombre.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase() === c) {
          return true;
        }
      }
    }

    return false;
  }

  ngOnDestroy(): void {
    this.suscripcionCaptcha.unsubscribe();
  }
}
