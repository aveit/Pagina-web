import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioComponent } from './inicio/inicio.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { QueEsAveitComponent } from './que-es-aveit/que-es-aveit.component';
import { ViajeComponent } from './viaje/viaje.component';
import { PreinscripcionComponent } from './preinscripcion/preinscripcion.component';
import { VerifEmailComponent } from './verif-email/verif-email.component';
import { CulturaAveitComponent } from './cultura-aveit/cultura-aveit.component';
import { RifaComponent } from './rifa/rifa.component';
import { NuestraHistoriaComponent } from './nuestra-historia/nuestra-historia.component';


const routes: Routes = [
  { path: '',         component: InicioComponent },
  { path: 'nosotros', component: NosotrosComponent },  
  { path: 'que-es-aveit', component: QueEsAveitComponent },
  { path: 'viaje', component: ViajeComponent },
  { path: 'preinscripcion', component: PreinscripcionComponent },
  { path: 'verificar', component: VerifEmailComponent },
  { path: 'cultura-aveit', component: CulturaAveitComponent },
  { path: 'rifa', component: RifaComponent },
  { path: 'historia', component: NuestraHistoriaComponent },
  { // Si se pone cualquier otra cosa redirige a inicio
    path: '**',
    redirectTo: ''
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
