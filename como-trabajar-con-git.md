# Como trabajar con git
Aqui se describe la metodologia de trabajo para no armar bolonqui en el repo (igual algun que otro se puede armar) <br>
Como no conozco programas con interfaz describo como trabajar desde la consola de Linux o Windows.

## Clonar el repo en tu compu
Poner el siguiente comando:
```
git clone https://gitlab.com/aveit/Pagina-web.git
```
Te pedira tu mail y clave de gitlab.

## (Si ya lo tenes clonado)
Si clonaste el repo en tu compu hace un tiempo, antes de comenzar a trabajar es necesario
obtener la ultima version, <br> 
para esto se hace
```
git pull
```
Si hubo modificaciones va a tirar algo como esto, 
```
remote: Enumerating objects: 98, done.
remote: Counting objects: 100% (98/98), done.
remote: Compressing objects: 100% (84/84), done.
remote: Total 94 (delta 9), reused 87 (delta 7)
Unpacking objects: 100% (94/94), done.
De https://gitlab.com/aveit/Pagina-web
   3b48bc8..4869a0f  master     -> origin/master
Updating 3b48bc8..4869a0f
Fast-forward
```
Seguido de los cambios entre la version que tenias y la que acabas de bajar. 

Si no hay cambios entre la version que esta en tu compu te tira:
```
Already up-to-date.
```

## Comenzar a trabajar
Antes de comenzar a trabajar es importante crear una rama y despues mergearla (unirla)
a la rama principal. <br>
Para esto se hace, por ejemplo:
```
git checkout -b nombreRama
```
Luego comienzo a trabajar.

## Subir modificaciones
Una vez que modifique archivos para subirlos se sigue los siguientes pasos (por ejemplo).
```
git add archivoModificado.html otroArchivoModificado.css
git add NombreCarpeta/
```
luego se hace un commit con un mensaje sobre lo que modificaste.
```
git commit -m "Hice el titulo mas copado"
```
Hasta aqui todo esto esta registrado en tu compu. Para subirlo a gitlab y que los demas puedan verlo hay que hacer.
```
git push origin nombreRama
```
De nuevo te pedira credenciales de Gitlab a menos que hayas configurado el SSH.

**NOTA: siempre es bueno hacer los commit lo mas atomico posible, modificaste el titulo entonces comitteas, agregaste imagenes, commiteas y asi**

## Mergear una rama
Cuando tu trabajo este listo y todos los commits pusheados, tenes que hacer un merge asi tus cambios se unen a la rama principal.
Para esto ir a
```
https://gitlab.com/aveit/Pagina-web/branches
```
Aqui hacer:
- Buscar tu rama y hacer click en el boton "Merge Request".
- Se abrira una nueva pagina y ahi agregas un comentario sobre lo que hiciste o falta hacer, o lo que pinte.
- Luego haces click en "Submit mergue request"
- Se abre otra ventana, aca tildar el checkbox "Remove source branch"
- Hacer Clck en "Merge" y listo! 


