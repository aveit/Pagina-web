# Repositorio para Pagina web de Aveit

En este Repo se guardaran las paginas estaticas que se diseñen para Aveit, 

Como por ejemplo:
- Cuna de emprendedores
- Maraton
- Wiki
- Sitio WEB provisorio de AVEIT
- Cualquier otra pagina que se quiera subir

El proposito de esto, es evitar andar compartiendo por drive, pendrive, etc. 
Asi todos podemos ver, y probar en todo momento que se esta haciendo. Tambien
asi reportar problemas diretamente en "issues"

## Organizacion del repo
La estrutura contempla un directorio por cada evento,
y dentro estos, una serie de directorios para las paginas
correspondientes a las distintas ediciones del evento.

Por ejemplo:

/Pagina-web
- /cuna-de-emprendedores
    - /2018
        - /css
        - /img
        - index.html 
    - /2017
        - /css
        - /img
        - index.html 
- /maraton
    - /2018
        - /css
        - /img
        - index.html 
    - /2017
        - /css
        - /img
        - index.html

- /sitio-provisorio-aveit
    - /assets
	- /css
	- /demo
	- /fonts
	- /img
	- /js
	- /scss
    - /docs
    - CHANGELOG.md
    - gulpfile.js
    - index.html
    - nostros.html
    - package.json
     

## Herramientas necesarias
Se necesitan las siguientes cosas.
### git en linux
En el caso de usuarios Linux ya viene instalado, igualmente es bueno comprobarlo
```
git --version
```
si no esta instalado, ejecutar el siguiente comando.
```
sudo apt-get install git
```
### git en Windows
Descargarlo de la siguiente página

https://git-scm.com/download/win

### git en MAC
Ca-re-toooon

### Editor de archivos .md
Los archivos .md se utilizan para documentar (por ejemplo esto que estas leyendo es .md)
Si no se la tiene clara con la sintaxis, es bueno utilizar algun interprete, como por ejemplo:

https://dillinger.io/

### Consola de navegadores
Es importante que cuando no te funcione algo, y no sepas porque, utilices el inspector de los navegadores
```
click derecho -> inspeccionar elemento
```
Ya que aqui, sobretodo en la solapa "consola" o "red", se obtiene buena información.

__IMPORTANTE: cuidado con usar http:complemento_css, va a funcionar abriendo la pagina__
__desde un archivo de tu PC, pero cuando lo subas al server, todos lso complementos que__
__sean buscados con http no se cargaran. Para evitar eso buscarlos con https__

si queres saber mas de este error podes leer: 

https://developer.mozilla.org/en-US/docs/Web/Security/Mixed_content
          

## Authors
* **Sofía Toselli** - *Maintainer* - sttoselli@gmail.com
* **Antonino Gratton** - *Maintainer* - antonino.gratton@gmail.com
* **Ailen Bernardi** - *Developer* - ailenbernardi94@gmail.com 
* **Mara Gomez** - *Developer* - marisol.gomez.mg97@gmail.com
* **Joaco Paviolo** - *Developer* - joacopaviolo1@gmail.com
* **Luca Guazzaroni** - *abri el repo* - lucaguazzaroni@gmail.com

