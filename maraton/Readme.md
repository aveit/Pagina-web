# Como subir pagina de maraton al server
se detalla como actualizar la pagina de maraton en el server

## ingresar al server

Al server se puede ingresar de dos formas:

#### 1.Por ssh desde tu PC

Si dispones de un usuario haces el comando
```
ssh usuario@192.168.2.176
```
te pedira la contraseña del usuario


#### 2.Desde el servidor en si

Pones usuario y password y listorti.


## Actualizar pagina

Si ya se encuentra clonado el repo ir al directorio
Ir a la siguiente direccion
```
cd /var/www/html/Pagina-web
```
y luego hacer 
```
git pull
```

## Descripcion del nginx
nginx Se encarga de redireccionar las peticiones al archivo html en cuestion.
Esta configuracion esta en el archivo
```
cd /etc/nginx/sites-enabled/
```
Abrir el archivo "default"

## Como apuntar a la version actual

La version actual de cuna se apunta con la url
```
aveit.frc.utn.edu.ar/maraton 
```
Aqui la config de nginx que hace que apunte al año en cuestion
```
location /maraton {
    alias /var/www/html/Pagina-web/maraton/2017;
    autoindex on;
}
```
Asi que si se hace una carpeta para la pagina de 2018, se tendria que cambiar a:
```
location /maraton {
    alias /var/www/html/Pagina-web/maraton/2018;
    autoindex on;
}
```
Luego de hacer un cambio en este archivo se debe reiniciar nginx, haciendo
```
systemctl stop nginx

```
y por ultimo
```
systemctl start nginx
```
Esperar un timepito y tendria que estar funcionando. Tener atencion a que si no se reflejan los
cambios puede ser por el cache del navegador